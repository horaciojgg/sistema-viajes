
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./Assets/css/bootstrap.css">
    <link rel="stylesheet" href="./Assets/css/normalize.css">
    <link rel="stylesheet" href="./Assets/css/estilos.css">
    <link rel="stylesheet" href="./Assets/css/viajes.css">
    <script src="./Assets/js/jquery.js"></script>
    <script src="./Assets/js/bootstrap.js"></script>
    <script src="./Assets/js/viajes.js"></script>
    <title>Document</title>
</head>
<body>
    <div>
    <div  class="d-flex justify-content-between navbar">    
        <h1>Viajes Inc.</h1>
        <div class="d-flex flex-row justify-content-between" style="width:35%;">
            <span><a href="index.php">Inicio</a></span>
        </div>
    </div>
    </div>
    <div>
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h1 style="margin:25px;">Bienvenido al módulo de gestión de viajes.</h1>
        </div>
    </div>
        <?php
        if (!empty($viajes))
        { echo '<div class="table-responsive tabla">
            <table class="table table-striped table-hover">
            <thead>
                <th>Código</th>
                <th>Plazas</th>
                <th>Origen</th>
                <th>Destino</th>
                <th>Precio</th>
                <th class="">Acciones</th>
            </thead>
            <tbody>';
            foreach ($viajes as $viaje)
            {
               echo '
               <tr> <td>'. $viaje["codigo"] . '</td>
               <td>'. $viaje["plazas"] . '</td>
               <td>'. $viaje["origen"] . '</td>
               <td>'. $viaje["destino"] . '</td>  
               <td>'. $viaje["precio"] . '</td>  
               <td>
                    <button onclick="eliminar('. $viaje["id"].')" type="button" class="btn btn-danger">Eliminar</button> 
                    <button onclick="editar('. $viaje["id"] . ')"type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-editar">Editar</button>
                </td>
               </tr>';
            }
            echo '</tbody>
            </table>';
        } else 
        {
            echo '<div class="d-flex justify-content-center"><h2>Aún no hay viajes para mostrar.</h2></div>';
        }
        ?>

    </div>
    <div id="crear" class="boton-crear-wrapper">
        <button type="button" class="boton-crear" data-toggle="modal" data-target="#modal-crear"><h1 class="text-white">+</h1></button>
    </div>

<div class="modal fade" id="modal-crear" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Insertar Viaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="viajes.php" method="POST">
                <div class="form-group">
                    <label for="input-codigo-crear">Código:</label>
                    <input id="input-codigo-crear" type="text" class="form-control" name="codigo" placeholder="Código" required>
                </div>
                <div class="form-group">
                    <label for="input-plazas-crear">Plazas:</label>
                    <input id="input-cedula-crear" type="number" class="form-control" name="plazas" placeholder="Plazas" required>
                </div>
                <div class="form-group">
                    <label for="input-orige-crear">Origen:</label>
                    <input id="input-origen-crear" type="text" class="form-control" name="origen" placeholder="Origen" required>
                </div>
                <div class="form-group">
                    <label for="input-destino-crear">Destino:</label>
                    <input id="input-destino-crear" type="text" class="form-control" name="destino" placeholder="Destino">
                </div> 
                <div class="form-group">
                    <label for="input-destino-crear">Precio</label>
                    <input id="input-precio-crear" type="number" class="form-control"name="precio" placeholder="Precio">
                </div>      
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary" name="crear" >Insertar</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Viaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="viajes.php" method="POST">
            <div class="form-group">
                <label for="input-codigo-crear">Código:</label>
                <input id="input-codigo-crear" type="text" class="form-control" name="codigo" placeholder="Código" required>
            </div>
            <div class="form-group">
                <label for="input-plazas-crear">Plazas:</label>
                <input id="input-cedula-crear" type="number" class="form-control" name="plazas" placeholder="Plazas" required>
            </div>
            <div class="form-group">
                <label for="input-orige-crear">Origen:</label>
                <input id="input-origen-crear" type="text" class="form-control" name="origen" placeholder="Origen" required>
            </div>
            <div class="form-group">
                <label for="input-destino-crear">Destino:</label>
                <input id="input-destino-crear" type="text" class="form-control" name="destino" placeholder="Destino">
            </div> 
            <div class="form-group">
                <label for="input-destino-crear">Precio</label>
                <input id="input-precio-crear" type="number" class="form-control"name="precio" placeholder="Precio">
            </div>   
            <input type="hidden" name="id" id="input-id-editar">       
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-info" name="editar" >Editar</button>
      </div>
      </form>
    </div>
  </div>
</div>


</body>
</html> 