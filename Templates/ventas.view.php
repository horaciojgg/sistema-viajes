
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./Assets/css/bootstrap.css">
    <link rel="stylesheet" href="./Assets/css/normalize.css">
    <link rel="stylesheet" href="./Assets/css/estilos.css">
    <link rel="stylesheet" href="./Assets/css/ventas.css">
    <script src="./Assets/js/jquery.js"></script>
    <script src="./Assets/js/bootstrap.js"></script>
    <script src="./Assets/js/ventas.js"></script>
    <title>Document</title>
</head>
<body>
    <div>
    <div  class="d-flex justify-content-between navbar">    
        <h1>Viajes Inc.</h1>
        <div class="d-flex flex-row justify-content-between" style="width:35%;">
            <span><a href="index.php">Inicio</a></span>
        </div>
    </div>
    </div>
    <div>
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h1 style="margin:25px;">Bienvenido al módulo de gestión de ventas.</h1>
        </div>
    </div>
        <?php
        if (!empty($ventas))
        { echo '<div class="table-responsive tabla">
            <table class="table table-striped table-hover">
            <thead>
                <th>Nombre Viajero</th>
                <th>Cedula Viajero</th>
                <th>Código Viaje</th>
                <th>Precio Viaje</th>
                <th>Plazas Asignadas</th>
                <th class="">Acciones</th>
            </thead>
            <tbody>';
            foreach ($ventas as $venta)
            {
               echo '<tr> <td>'. $venta["nombre_viajero"] . '</td>
               <td>'. $venta["cedula_viajero"] . '</td>
               <td>'. $venta["codigo_viaje"] . '</td>
               <td>'. $venta["precio_viaje"] . '</td>
               <td>'. $venta["plazas_asignadas"].'</td>  
               <td>
                    <button onclick="eliminar('. $venta["id"].')" type="button" class="btn btn-danger">Eliminar</button> 
                </td>
               </tr>';
            }
            echo '</tbody>
            </table>';
        } else 
        {
            echo '<div class="d-flex justify-content-center"><h2>Aún no se ha realizado ninguna venta.</h2></div>';
        }
        ?>

    </div>
    <div id="crear" class="boton-crear-wrapper">
        <button type="button" class="boton-crear" data-toggle="modal" data-target="#modal-crear"><h1 class="text-white">+</h1></button>
    </div>

<div class="modal fade" id="modal-crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Asignar Plaza a Viajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="ventas.php" method="POST">
        <div class="form-group">
        <label for="combo-viajeros-insertar">Viajeros:</label>
            <select class="form-control" name="viajero_id" id="combo-viajeros-insertar">
                <?php
                    foreach ($viajeros as $viajero)
                    {
                        echo '<option value ="'.$viajero["id"].'">'. 'Nombre: ' .$viajero["nombre"].' - Ci: '. $viajero["cedula"] .'</option>';
                    }
                ?>
            </select>
            <label for="combo-viajes-insertar">Viajes:</label>
            <select class="form-control" name="viaje_id" id="combo-viajes-insertar">
                <?php
                    foreach ($viajes as $viaje)
                    {
                        echo '<option value ="'.$viaje["id"].'">'.'Código: '.$viaje["codigo"]. ' - Origen: '.
                        $viaje["origen"] . ' - Destino: '. $viaje["destino"]. ' - Plazas disponibles : '. $viaje["plazas"] .'</option>';
                    }
                ?>
            </select>
            <label for="plazas-asignar">Plazas a asignar:</label>
            <input id="plazas-asignar" min="1" type="number" class="form-control" name="plazas_asignadas">
        </div>          
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary" name="crear" >Asignar</button>
      </div>
      </form>
    </div>
  </div>
</div>
</body>
</html> 