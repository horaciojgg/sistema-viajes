# sistema-viajes
Hola! Esta en la guía para instalar el proyecto del sistema de viajes.

# Base de datos

Crea una base de datos con el nombre "viajes" y corre el siguiente script:
```
CREATE  TABLE  `viajero` (

`cedula`  int(8) NOT  NULL,

`nombre`  text  NOT  NULL,

`direccion`  text  NOT  NULL,

`telefono`  text  NOT  NULL,

`id`  int(11) NOT  NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE  TABLE  `viajero_viajes` (

`id`  int(11) NOT  NULL,

`viaje_id`  int(11) NOT  NULL,

`viajero_id`  int(11) NOT  NULL,

`plazas_asignadas`  int(11) NOT  NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE  TABLE  `viajes` (

`codigo`  varchar(10) NOT  NULL,

`plazas`  int(11) NOT  NULL,

`destino`  varchar(100) NOT  NULL,

`origen`  varchar(100) NOT  NULL,

`precio`  decimal(10,0) NOT  NULL,

`id`  int(11) NOT  NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

