<?php
require_once 'autoload.php';


$dbHandler = new Viajes\Database\DatabaseHandler();
$managerVentas = new Viajes\Managers\ManagerVentas($dbHandler);
$managerViajeros = new Viajes\Managers\ManagerViajeros($dbHandler);
$managerViajes = new Viajes\Managers\ManagerViajes($dbHandler);

//Manejo de inserción del registro.
if (isset($_POST["crear"]))
{
    $viajeroId = $_POST["viajero_id"];
    $viajeId = $_POST["viaje_id"];
    $plazas = $_POST["plazas_asignadas"];

    $parametros = [
        "viajero_id" => $viajeroId,
        "viaje_id" => $viajeId,
        "plazas_asignadas" => $plazas
    ];
    $managerVentas->crear($parametros);
}

//Manejo de eliminación del registro.
if (isset($_POST["eliminar"]))
{
    $ventaId = $_POST["id"];
    $managerVentas->eliminar($ventaId);
}


$viajeros = $managerViajeros->consultarTodos();
$viajes = $managerViajes->consultarTodos();

$lista_ids = $managerVentas->consultarTodos();

foreach ($lista_ids as $id)
{
    $viajero_var = $managerViajeros->consultar($id["viajero_id"]);
    $viaje_var = $managerViajes->consultar($id["viaje_id"]);
    $venta_var = $managerVentas->consultar($id["id"]); 
    $ventas[] = [
        "id" => $id["id"],
        "nombre_viajero" => $viajero_var[0]["nombre"],
        "cedula_viajero" => $viajero_var[0]["cedula"],
        "codigo_viaje" => $viaje_var[0]["codigo"],
        "precio_viaje" => $viaje_var[0]["precio"],
        "plazas_asignadas" => $venta_var[0]["plazas_asignadas"]
    ];
}

include_once 'Templates/ventas.view.php';
