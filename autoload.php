<?php
spl_autoload_register('autoload');

function autoload($class, $dir = null)
{

    if(is_null($dir))
    {
        $dir =  "." . DIRECTORY_SEPARATOR;
    }

    foreach(scandir($dir) as $file){
        
        if(is_dir($dir.$file) && substr($file,0,1) !== '.' )
        {
        
            autoload($class, $dir.$file.DIRECTORY_SEPARATOR);
        }

        if(substr($file,0,2) !== '.__' && preg_match("/.php$/i",$file))
        {
            $limit = explode("\\", $class);
            if(str_replace('.php', '', $file) == $limit[count($limit)-1])
            {
                require_once( $dir . $file);
            }
        }
    }
}