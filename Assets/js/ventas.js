function eliminar(id) 
{
    var confirmado = confirm("¿Estas seguro que deseas eliminar esta venta?");
    var data = {
        'id': id,
        'eliminar': 'eliminar'
    };
    if (confirmado) {
     $.post('ventas.php', data, function() {
         location.reload();
         alert('La venta se ha eliminado con éxito!');
     });
    }
}