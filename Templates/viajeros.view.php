
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./Assets/css/bootstrap.css">
    <link rel="stylesheet" href="./Assets/css/normalize.css">
    <link rel="stylesheet" href="./Assets/css/estilos.css">
    <link rel="stylesheet" href="./Assets/css/viajeros.css">
    <script src="./Assets/js/jquery.js"></script>
    <script src="./Assets/js/bootstrap.js"></script>
    <script src="./Assets/js/viajeros.js"></script>
    <title>Document</title>
</head>
<body>
    <div>
    <div  class="d-flex justify-content-between navbar">    
        <h1>Viajes Inc.</h1>
        <div class="d-flex flex-row justify-content-between" style="width:35%;">
            <span><a href="index.php">Inicio</a></span>
        </div>
    </div>
    </div>
    <div>
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h1 style="margin:25px;">Bienvenido al módulo de gestión de viajeros.</h1>
        </div>
    </div>
        <?php
        if (!empty($viajeros))
        { echo '<div class="table-responsive tabla">
            <table class="table table-striped table-hover">
            <thead>
                <th>Nombre</th>
                <th>Cedula</th>
                <th>Telefono</th>
                <th>Dirección</th>
                <th class="">Acciones</th>
            </thead>
            <tbody>';
            foreach ($viajeros as $viajero)
            {
               echo '<tr> <td>'. $viajero["nombre"] . '</td>
               <td>'. $viajero["cedula"] . '</td>
               <td>'. $viajero["telefono"] . '</td>
               <td>'. $viajero["direccion"] . '</td>  
               <td>
                    <button onclick="eliminar('. $viajero["id"].')" type="button" class="btn btn-danger">Eliminar</button> 
                    <button onclick="editar('. $viajero["id"] . ')"type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-editar">Editar</button>
                </td>
               </tr>';
            }
            echo '</tbody>
            </table>';
        } else 
        {
            echo '<div class="d-flex justify-content-center"><h2>Aún no hay pasajeros para mostrar.</h2></div>';
        }
        ?>

    </div>
    <div id="crear" class="boton-crear-wrapper">
        <button type="button" class="boton-crear" data-toggle="modal" data-target="#modal-crear"><h1 class="text-white">+</h1></button>
    </div>

<div class="modal fade" id="modal-crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Insertar Viajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="viajeros.php" method="POST">
                <div class="form-group">
                    <label for="input-nombre">Nombre:</label>
                    <input id="input-nombre-crear" maxlength="100" type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="input-cedula-crear">Cédula:</label>
                    <input id="input-cedula-crear" maxlength="8" type="text" class="form-control" name="cedula" placeholder="Cédula" required>
                </div>
                <div class="form-group">
                    <label for="input-tlf-crear">Teléfono</label>
                    <input id="input-tlf-crear" maxlength="11" type="tel" class="form-control" name="telefono" placeholder="Teléfono" required>
                </div>
                <div class="form-group">
                    <label for="input-direccion-crear">Dirección</label>
                    <textarea class="form-control" maxlength="3000" placeholder="Dirección" name="direccion" id="input-direccion-crear" cols="50" rows="1" required></textarea>
                </div>       
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-primary" name="crear" >Insertar</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Viajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="viajeros.php" method="POST">
                <div class="form-group">
                    <label for="input-nombre">Nombre:</label>
                    <input id="input-nombre-editar" maxlength="100" type="text" class="form-control" name="nombre" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="input-cedula-crear">Cédula:</label>
                    <input id="input-cedula-editar" maxlength="8" type="number" class="form-control" name="cedula" placeholder="Cédula" required>
                </div>
                <div class="form-group">
                    <label for="input-tlf-crear">Teléfono</label>
                    <input id="input-tlf-editar" maxlength="11" type="tel" class="form-control" name="telefono" placeholder="Teléfono" required>
                </div>
                <div class="form-group">
                    <label for="input-direccion-editar">Dirección</label>
                    <textarea class="form-control" maxlength="3000" placeholder="Dirección" name="direccion" id="input-direccion-editar" cols="50" rows="1" required></textarea>
                </div>
                <input type="hidden" name="id" id="input-id-editar">       
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-info" name="editar" >Editar</button>
      </div>
      </form>
    </div>
  </div>
</div>


</body>
</html> 