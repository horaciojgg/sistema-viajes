function eliminar(id) {
   var confirmado = confirm("Estas seguro que deseas eliminar este pasajero?");
   var data = {
       'id': id,
       'eliminar': 'eliminar'
   };
   if (confirmado) {
    $.post('viajeros.php', data, function() {
        location.reload();
        alert('Viajero eliminado con éxito!');
    });
   }
}

function editar(id) {
    $('#modal-editar').modal('show');
    $('#input-id-editar').val(id);
}
