<?php 

namespace Viajes\Managers;

use Viajes\Database\DatabaseHandlerInterface;
use Viajes\Models\Viajero;
use Exception;

class ManagerViajeros implements ManagerViajerosInterface
{
    const NOMBRE_TABLA = "viajero";
    private $dbHandler;

    public function __construct(DatabaseHandlerInterface $dbHandler)
    {
        $this->dbHandler = $dbHandler;
    }

    public function consultarTodos(): array
    {
        return $this->dbHandler->consultarTabla(self::NOMBRE_TABLA);
    }

    public function crear(Viajero $viajero): bool
    {   
        $parametros = [
            "nombre" => $viajero->getNombre(),
            "cedula" => $viajero->getCedula(),
            "telefono" => $viajero->getTelefono(),
            "direccion" => $viajero->getDireccion(),
        ];

        try 
        {
            $this->dbHandler->crear(self::NOMBRE_TABLA, $parametros);
            return true;
        } catch (Exception $e)
        {
            return false;
        }
    }

    public function eliminar(int $id): bool
    {
        return $this->dbHandler->eliminar(self::NOMBRE_TABLA, $id);
    }

    public function editar(int $id, Viajero $viajero):bool 
    {
        $parametros = [
            "nombre" => $viajero->getNombre(),
            "cedula" => $viajero->getCedula(),
            "telefono" => $viajero->getTelefono(),
            "direccion" => $viajero->getDireccion(),
        ];

        return $this->dbHandler->actualizar(self::NOMBRE_TABLA, $parametros, $id);
    }

    public function consultar(int $id): array
    {
        return $this->dbHandler->consultarRegistro(self::NOMBRE_TABLA,$id);
    }
}