<?php

namespace Viajes\Managers;

use Viajes\Database\DatabaseHandlerInterface;

class ManagerVentas implements ManagerVentasInterface
{
    const NOMBRE_TABLA = "viajero_viajes";
    private $dbHandler;

    public function __construct(DatabaseHandlerInterface $dbHandler)
    {
        $this->dbHandler = $dbHandler;
    }

    public function crear(array $parametros): bool
    {
        return $this->dbHandler->crear(self::NOMBRE_TABLA, $parametros);
    }

    public function consultar(int $id): array
    {
        return $this->dbHandler->consultarRegistro(self::NOMBRE_TABLA, $id);
    }

    public function consultarTodos(): array
    {
        return $this->dbHandler->consultarTabla(self::NOMBRE_TABLA);

    }

    public function eliminar(int $id): bool
    {
        return $this->dbHandler->eliminar(self::NOMBRE_TABLA, $id);
        
    }
}