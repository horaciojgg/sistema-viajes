
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./Assets/css/bootstrap.css">
    <link rel="stylesheet" href="./Assets/css/normalize.css">
    <link rel="stylesheet" href="./Assets/css/estilos.css">
    <script src="./Assets/js/jquery.js"></script>
    <script src="./Assets/js/index.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="landing" >
    <div class="d-flex justify-content-between navbar">    
        <h1>Viajes Inc.</h1>
        <div class="d-flex flex-row justify-content-between" style="width:35%;">
            <span><a href="index.php">Inicio</a></span>
        </div>
    </div>
    <div class="d-flex justify-content-center center">
        <div>
            <h1 class="text-white">Bienvenido a Viajes.com</h1>
        </div>
    </div>
    <div class="d-flex justify-content-center bottom bg-gray">
        <div>
            <h2 class="text-white"><a href="#main-menu">Continuar</a></h2>
        </div>
    </div>
    </div>


    <div id="main-menu" class="main d-flex justify-content-around align-items-center">
        <div id="boton-pasajeros" class="large-button d-flex justify-content-center flex-column align-items-center">
            <h2 class="text-white medium-font">Viajeros</h2>
            <p>Entrar al módulo de manejo de Viajes.</p>
        </div>
        <div id="boton-viajes" class="large-button d-flex justify-content-center flex-column align-items-center">
            <h2 class="text-white medium-font">Viajes</h2>
            <p>Entrar al módulo de manejo de viajes.</p>
        </div>
        <div id="boton-ventas" class="large-button d-flex justify-content-center flex-column align-items-center">
            <h2 class="text-white medium-font">Ventas</h2>
            <p>Entrar al módulo de manejo de ventas.</p>
        </div>
    </div>
</body>

</html>