<?php
require_once 'autoload.php';
$dbHandler = new Viajes\Database\DatabaseHandler;
$managerViajeros = new Viajes\Managers\ManagerViajeros($dbHandler);

//Manejo de inserción del registro.
if(isset($_POST["crear"]))
{
    $nombre = $_POST["nombre"];
    $cedula = $_POST["cedula"];
    $telefono = $_POST["telefono"];
    $direccion = $_POST["direccion"];

    $viajero = new Viajes\Models\Viajero($nombre, $cedula, $telefono, $direccion);

    $resultado = $managerViajeros->crear($viajero);
    unset($_POST["crear"]);
}

//Manejo de eliminación del registro. 
if (isset($_POST["eliminar"]))
{
    $id = $_POST["id"];
    $managerViajeros->eliminar($id);
}

//Manejo de edición/actualización del registro.
if (isset($_POST["editar"]))
{
    $id = $_POST["id"];
    
    $nombre = $_POST["nombre"];
    $cedula = $_POST["cedula"];
    $telefono = $_POST["telefono"];
    $direccion = $_POST["direccion"];

    $viajero = new Viajes\Models\Viajero($nombre, $cedula, $telefono, $direccion);
    $managerViajeros->editar($id, $viajero);
}


$viajeros = $managerViajeros->consultarTodos();

include_once 'Templates/viajeros.view.php';

