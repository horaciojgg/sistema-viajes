<?php

namespace Viajes\Models;

class Viaje
{
    private $codigo;
    private $plazas;
    private $destino;
    private $origen;
    private $precio;

    public function __construct(string $codigo, int $plazas, string $origen,
    string $destino, float $precio)
    {
        $this->codigo = $codigo;
        $this->plazas = $plazas;
        $this->destino = $destino;
        $this->origen = $origen;
        $this->precio = $precio;
        
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getPlazas()
    {
        return $this->plazas;

    }

    public function getOrigen()
    {
        return $this->origen;

    }

    public function getDestino()
    {
        return $this->destino;

    }

    public function getPrecio()
    {
        return $this->precio;

    }

}