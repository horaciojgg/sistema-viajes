<?php

namespace Viajes\Managers;

use Viajes\Models\Viajero;

interface ManagerViajerosInterface 
{
    public function crear(Viajero $viajero): bool;
    public function eliminar(int $id): bool;
    public function consultar(int $id): array;
    public function editar(int $id, Viajero $viajero): bool;
    public function consultarTodos(): array;
}