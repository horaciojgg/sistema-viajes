<?php

namespace Viajes\Models;

class Viajero
{
    private $nombre;
    private $cedula;
    private $telefono;
    private $direccion;

    public function __construct(string $nombre, int $cedula, string $telefono, string $direccion)
    {
        $this->nombre = $nombre;
        $this->cedula = $cedula;
        $this->telefono = $telefono;
        $this->direccion = $direccion;

    }

    public function getNombre(): string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }

    public function getCedula(): int
    {
        return $this->cedula;
    }

    public function setCedula(int $cedula)
    {
        $this->cedula = $cedula;
    }

    public function getTelefono(): string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono)
    {
        $this->telefono = $telefono;
    }

    public function getDireccion(): string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion)
    {
        $this->direccion = $direccion;
    }


}