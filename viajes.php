<?php
require_once 'autoload.php';
$dbHandler = new Viajes\Database\DatabaseHandler;
$managerViajes = new Viajes\Managers\ManagerViajes($dbHandler);

//Manejo de inserción del registro.
if(isset($_POST["crear"]))
{
    $codigo = $_POST["codigo"];
    $plazas = $_POST["plazas"];
    $origen = $_POST["origen"];
    $destino = $_POST["destino"];
    $precio = $_POST["precio"];

    $viaje = new Viajes\Models\Viaje($codigo, $plazas, $origen, $destino, $precio);

    $resultado = $managerViajes->crear($viaje);
}

//Manejo de eliminación del registro. 
if (isset($_POST["eliminar"]))
{
    $id = $_POST["id"];
    $managerViajes->eliminar($id);
}

//Manejo de edición/actualización del registro.
if (isset($_POST["editar"]))
{
    $id = $_POST["id"];

    $codigo = $_POST["codigo"];
    $plazas = $_POST["plazas"];
    $origen = $_POST["origen"];
    $destino = $_POST["destino"];
    $precio = $_POST["precio"];

    $viaje = new Viajes\Models\Viaje($codigo, $plazas, $origen, $destino, $precio);
    
    $managerViajes->editar($id, $viaje);
}


$viajes = $managerViajes->consultarTodos();

include_once 'Templates/viajes.view.php';

