<?php 

namespace Viajes\Managers;

use Viajes\Database\DatabaseHandlerInterface;
use Viajes\Models\Viaje;
use Exception;

class ManagerViajes implements ManagerViajesInterface
{
    const NOMBRE_TABLA = "viajes";
    private $dbHandler;

    public function __construct(DatabaseHandlerInterface $dbHandler)
    {
        $this->dbHandler = $dbHandler;
    }

    public function consultarTodos(): array
    {
        return $this->dbHandler->consultarTabla(self::NOMBRE_TABLA);
    }

    public function crear(Viaje $viaje): bool
    {   
        $parametros = [
            "codigo" => $viaje->getCodigo(),
            "plazas" => $viaje->getPlazas(),
            "origen" => $viaje->getOrigen(),
            "destino" => $viaje->getDestino(),
            "precio" => $viaje->getPrecio(),
        ];

        try 
        {
            $this->dbHandler->crear(self::NOMBRE_TABLA, $parametros);
            return true;
        } catch (Exception $e)
        {
            return false;
        }
    }

    public function eliminar(int $id): bool
    {
        return $this->dbHandler->eliminar(self::NOMBRE_TABLA, $id);
    }

    public function editar(int $id, Viaje $viaje):bool 
    {
        $parametros = [
            "codigo" => $viaje->getCodigo(),
            "plazas" => $viaje->getPlazas(),
            "origen" => $viaje->getOrigen(),
            "destino" => $viaje->getDestino(),
            "precio" => $viaje->getPrecio(),
        ];
        return $this->dbHandler->actualizar(self::NOMBRE_TABLA, $parametros, $id);
    }

    public function consultar(int $id): array
    {
        return $this->dbHandler->consultarRegistro(self::NOMBRE_TABLA,$id);
    }
}