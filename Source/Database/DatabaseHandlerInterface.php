<?php

namespace Viajes\Database;

 interface DatabaseHandlerInterface 
{
    public function crear(string $tabla, array $parametros): bool;
    public function consultarTabla(string $table): array;
    public function eliminar(string $tabla, $key): bool;
    public function actualizar(string $tabla, array $parametros, int $id): bool;
    public function consultarRegistro(string $tabla, int $id): array;
}
