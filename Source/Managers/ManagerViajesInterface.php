<?php

namespace Viajes\Managers;

use Viajes\Models\Viaje;

interface ManagerViajesInterface 
{
    public function crear(Viaje $viaje): bool;
    public function eliminar(int $id): bool;
    public function consultar(int $id): array;
    public function editar(int $id, Viaje $viaje): bool;
    public function consultarTodos(): array;
}