function eliminar(id) {
    var confirmado = confirm("¿Estas seguro que deseas eliminar este viaje?");
    var data = {
        'id': id,
        'eliminar': 'eliminar'
    };
    if (confirmado) {
     $.post('viajes.php', data, function() {
         location.reload();
         alert('Viaje eliminado con éxito!');
     });
    }
 }
 
 function editar(id) {
     $('#modal-editar').modal('show');
     $('#input-id-editar').val(id);
 }
 
