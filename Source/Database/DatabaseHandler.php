<?php

namespace Viajes\Database;

 class DatabaseHandler implements DatabaseHandlerInterface 
{

    private $host = "localhost";
	private $user = "root";
	private $password = "";
	private $database = "viajes";
	private $connection = "";
	
	public function __construct() {
		$conn = $this->connectDB();
		$this->connection = $conn;
    }
    
    function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}

    public function crear(string $tabla, array $parametros): bool
    {
        $query = "INSERT INTO ". $tabla ." (";

        foreach ($parametros as $columna => $valor)
        {
            $query = $query . $columna. ", ";
        }
        $query = rtrim($query,", ") . ") VALUES (";

        foreach ($parametros as $columna => $valor)
        {
            $query = $query . '"'. $valor. '"'. ", ";
        }
        $query = rtrim($query,", ") . ")";

        try {
			mysqli_query($this->connection, $query);
			return true;
		} catch(Exception $err)
		{
			return false;
		}
    }

    public function eliminar(string $tabla, $id): bool
    {
        $query = "DELETE FROM " . $tabla . " WHERE id = " . $id;
        try{
			mysqli_query($this->connection, $query);
			return true;
		}
		 catch(Exception $err)
		{
			return false;
		}
    }

    public function actualizar(string $tabla, array $parametros, int $id): bool
    {
        $query = "UPDATE " .$tabla." SET ";
		
		foreach($parametros as $columna => $valor)
		{
			$query .=   $columna . ' = "' . $valor. '", ';
		}

        $query = rtrim($query,', ');
        
        $query .= " WHERE id = " .$id;

		try{
			mysqli_query($this->connection, $query);
			return true;
		}
		 catch(Exception $err)
		{
			return false;
		}

    }

    public function consultarTabla(string $tabla): array
    {
        $query = "SELECT * FROM " . $tabla;
		$result = mysqli_query($this->connection, $query);
		return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function consultarRegistro(string $tabla, int $id): array
    {
        $query = "SELECT * FROM " . $tabla . " WHERE id = " . $id;
		$result = mysqli_query($this->connection, $query);
		return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

}