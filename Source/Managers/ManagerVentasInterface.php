<?php

namespace Viajes\Managers;

interface ManagerVentasInterface 
{
    public function crear(array $parametros): bool;
    public function consultar(int $id): array;
    public function consultarTodos(): array;
    public function eliminar(int $id): bool;
}